<?php
require_once __DIR__ . '/../../libs/_settings.php';

$fb = new Facebook\Facebook([
	'app_id' => FB_APP_ID,
	'app_secret' => FB_APP_SECRET,
	'default_graph_version' => 'v2.8'
]);

$helper = $fb->getJavaScriptHelper();

try {
	$accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
	// When Graph returns an error
	json_encode(array('error' => 'Graph returned an error: ' . $e->getMessage()));
	exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
	// When validation fails or other local issues
	json_encode(array('error' => 'Facebook SDK returned an error: ' . $e->getMessage()));
	exit;
}

if (!isset($accessToken)) {
	json_encode(array('error' => 'No cookie set or no OAuth data could be obtained from cookie.'));
	exit;
}

$_SESSION['fb_access_token'] = (string)$accessToken;

try {
	// Returns a `Facebook\FacebookResponse` object
	$response = $fb->get('/me?fields=id,cover,name,first_name,last_name, age_range, link,gender,locale,picture,timezone,updated_time,verified', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
	json_encode(array('error' => 'Graph returned an error: ' . $e->getMessage()));
	exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
	json_encode(array('error' => 'Facebook SDK returned an error: ' . $e->getMessage()));
	exit;
}

$user = $response->getGraphUser();
$id = $user->getId();
if (!$id) {
	json_encode(array('error' => 'Facebook ID invalid'));
	exit;
}
$picture = $user->getPicture();
$bday = $user->getBirthday();
$location = $user->getLocation();
$hometown = $user->getHometown();
$row = array(
	'id' => $id,
	'name' => $user->getName(),
	'picture' => $picture ? $picture->getUrl() : 'NULL',
	'first_name' => $user->getFirstName(),
	'middle_name' => $user->getMiddleName(),
	'last_name' => $user->getLastName(),
	'email' => $user->getEmail(),
	'gender' => $user->getGender(),
	'link' => $user->getLink(),
 	'birthday' => $bday ? $bday : 'NULL',
	'location_id' => $location ? $location->getId() : 0,
	'hometown_id' => $hometown ? $hometown->getId() : 0,
);
$exists = DB::query('SELECT id FROM fb_users WHERE id=' . $id);
if ($exists) {
	$row['chtime'] = 'NOW()';
	DB::update('fb_users', $id, $row);
} else {
	$row['crtime'] = 'NOW()';
	DB::insert('fb_users', $row);
}
$_SESSION['fb_user'] = $row;
echo json_encode($row);
