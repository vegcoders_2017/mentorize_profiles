<?php
require_once __DIR__ . '/../libs/_settings.php';
$user = @$_SESSION['fb_user'];
?><!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8"/>
	<title>Mentorize: учебный сайт</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
		  integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<a class="navbar-brand" href="/">Mentorize</a>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="/">Главная <span class="sr-only">(текущая)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/catalog">Каталог</a>
			</li>
		</ul>
		<form class="form-inline" id="navbar_login_block" <?php if ($user) echo ' style="display:none"' ?>>
			<a class="btn btn-outline-success" type="button" href="#" onClick="navbar_login(); return false;">Войти через Facebook</a>
		</form>
		<form class="form-inline" id="navbar_logout_block" <?php if (!$user) echo ' style="display:none"' ?>>
			<a id="navbar_profile_link" href="/profile" style="padding-right: 10px">
				<img src="<?php echo @$user['picture']; ?>" alt="profile"/>
				<span><?php echo @$user['name']; ?></span>
			</a>
			<a class="btn btn-outline-success" type="button" href="#" onClick="navbar_logout(); return false;">Выйти</a>
		</form>
	</div>
</nav>

<div class="container">
	Тут будет большой текст и картинки
</div>

<script>
	var FB_APP_ID = '<?php echo FB_APP_ID;?>';
</script>
<script src="./js/login.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
		integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
		integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>