navbar_login = function () {
	FB.login(function (response) {
		if (response.authResponse) {
			$('#navbar_login_block a').addClass('btn-info');
			$.ajax({
				type: 'POST',
				url: '/login/fb_callback.php',
				dataType: 'json',
				success: function (data) {
					$('#navbar_login_block a').removeClass('btn-info');
					if (typeof(data.error) != 'undefined') {
						alert(data.error);
					} else {
						$('#navbar_login_block').hide();
						$('#navbar_logout_block').show();
						$('#navbar_logout_block img').attr('src', data.picture);
						$('#navbar_logout_block span').html(data.name);
					}
				}
			});
		} else {
			alert('Что-то пошло не так - попробуйте еще раз.');
		}
	}, {scope: 'email,public_profile'});
	return false;
};
navbar_logout = function () {
	$('#navbar_logout_block a').addClass('btn-info');
	$.ajax({
		type: 'POST',
		url: '/login/logout_callback.php',
		dataType: 'json',
		success: function (data) {
			$('#navbar_logout_block a').removeClass('btn-info');
			if (typeof(data.error) != 'undefined') {
				alert(data.error);
			} else {
				$('#navbar_login_block').show();
				$('#navbar_logout_block').hide();
			}
		}
	});
};


window.fbAsyncInit = function () {
	FB.init({
		appId: FB_APP_ID,
		cookie: true,
		version: 'v2.8'
	});
};
(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));