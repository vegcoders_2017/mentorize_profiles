<?php
class DB
{
	const DB_HOST = DB_HOST;
	const DB_NAME = DB_NAME;
	const DB_USER = DB_USER;
	const DB_PASS = DB_PASS;


	private static $instance;
	private static $db_link;

	private function __construct()
	{
		self::$db_link = new mysqli(self::DB_HOST, self::DB_USER, self::DB_PASS, self::DB_NAME);
		if (mysqli_connect_errno() || !self::$db_link) {
			die('Could not connect to DB ' . mysqli_connect_error());
		}
		self::$db_link->query('SET NAMES utf8');
 	}
	public function __destruct()
	{
	 	if (self::$db_link){
			self::$db_link->close();
		}
	}
	public static function escape($str, $add_slashes = false)
	{
		if ($str == 'NOW()' || $str == 'NULL') {
			return $str;
		}
		if (!isset(self::$instance)){
			$c = __CLASS__;
			self::$instance = new $c;
		}
		$tmp = self::$db_link->real_escape_string(trim($str));
		if ($add_slashes) {
			$tmp = '\'' . $tmp . '\'';
		}
		return $tmp;
	}
 	public static function begin()
	{
		self::$db_link->autocommit(FALSE);
	}
	public static function commit()
	{
		self::$db_link->commit();
		self::$db_link->autocommit(TRUE);
	}
	public static function rollback()
	{
		self::$db_link->rollback();
		self::$db_link->autocommit(TRUE);
	}
	public static function query($query, $return = '')
	{
		if (!isset(self::$instance)){
			$c = __CLASS__;
			self::$instance = new $c;
		}
		if (is_array($query)){
			DB::begin();
			foreach ($query AS $one){
				$link = self::$db_link->query($one);
				if (!$link) {
					DB::rollback();
					return false;
				}
			}
			DB::commit();
			return true;
		}

		$start_time = microtime(true);
		$link = self::$db_link->query($query);
		$mtime = microtime(true);
		$exec_time = round($mtime - $start_time, 4);
		if (!$link){
			die($exec_time . ' sec ' . $query . ' ' . self::$db_link->error);
		}
		$num_rows = (int) @$link->num_rows;
		if ($return == 'link'){
			return $link;
		} elseif ($return == 'array'){
			//$result = $link->fetch_all(MYSQLI_ASSOC);
			$result = array();
			while($row = $link->fetch_assoc()){
				$result[] = $row;
			}
		} elseif ($return == 'alist'){
			$result = array();
			$func_name = 'fetch_assoc';
			if($row = $link->{$func_name}()){
				$fkey = array_keys($row);
				$fkey = array_shift($fkey);
				do {$result[$row[$fkey]] = $row;}while($row = $link->{$func_name}());
			}
		} elseif ($return == 'list' || $return == 'mlist'){
			$result = array();
			$func_name = ($return == 'list') ? 'fetch_row' : 'fetch_assoc';
			if($row = $link->{$func_name}()){
				$cn = count($row);
				if ($return == 'mlist'){
					if ($cn == 2) {
						do {
							$key = array_shift($row);
							$val = array_shift($row);
							$result[$key][$val] = $val;
						}while($row = $link->{$func_name}());
					} elseif ($cn == 3) {
						do {$result[(int)array_shift($row)][(int)array_shift($row)] = array_shift($row);}while($row = $link->{$func_name}());
					} else {
						do {$result[array_shift($row)][] = $row;}while($row = $link->{$func_name}());
					}
				} elseif ($cn == 1){
					do {$result[$row[0]] = $row[0];}while($row = $link->fetch_row());
				} elseif ($cn == 2){
					do {$result[$row[0]] = $row[1];}while($row = $link->fetch_row());
				} else {
					do {$result[array_shift($row)] = $row;}while($row = $link->{$func_name}());
				}
			}
		} elseif (is_object($return) || function_exists($return)) {
			$result = array();
			while($row = $link->fetch_assoc()) {
				$return($result, $row);
			}
			return $result;
		} elseif (strpos(strtolower(trim($query)), 'select') === 0){
			$row = $link->fetch_assoc();
			$result = $row;
			if ($row && count($row) == 1){
				$result = array_pop($row);
			}
		} else {
			return $link;
		}
		$link->close();
		return $result;
	}
	public static function insert_sql($table, $row) {
		$sql	= ' INSERT INTO ' . $table . ' (';
		$sql2	= '';
		foreach ($row AS $key => $val) {
			if ($sql2) {
				$sql  .= ', ';
				$sql2 .= ',';
			}
			$sql  .= '`' . $key . '`';
			if ($val != 'NULL' && $val != 'NOW()') {
				$sql2 .= DB::escape($val, true);
			} else {
				$sql2 .= $val;
			}
		}
		$sql .= ' ) VALUES ( ';
		$sql .= $sql2 .  ')';
		return $sql;
	}
	public static function update_sql($table, $edit_id, $row) {
		$sql	= ' UPDATE ' . $table . ' SET ';
		foreach ($row AS $key => $val) {
			$sql   .= ', `' . $key . '`=';
			if ($val != 'NULL' && $val != 'NOW()') {
				$sql .= DB::escape($val, true);
			} else {
				$sql .= $val;
			}
		}
		if (strpos($edit_id, 'WHERE') === false) {
			$sql .= ' WHERE id=' . $edit_id;
		} else {
			$sql .= ' ' . $edit_id;
		}
		return str_replace('SET ,', 'SET ', $sql);
	}
	public static function insert($table, $row, $return_id = false) {
		$res =  DB::query(DB::insert_sql($table, $row), 'link');
		if (!$return_id) return $res;
		return mysqli_insert_id(static::$db_link);
	}
	public static function update($table, $edit_id, $row) {
		return DB::query(DB::update_sql($table, $edit_id, $row), 'link');
	}
}