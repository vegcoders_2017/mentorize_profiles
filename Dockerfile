FROM amazonlinux

RUN yum install -y yum-plugin-ovl && yum clean all

# Install linux update, followed by GCC and Make
# RUN yum -y update && yum clean all
# RUN yum install -y gcc make && yum clean all
RUN yum install -y htop && yum clean all
RUN yum install -y mc && yum clean all
ENV TERM=xterm

# Install Nginx + PHP + Mysql
RUN yum install -y nginx && yum clean all

RUN yum install -y php70 && yum clean all

RUN yum install -y php70-fpm && yum clean all

RUN yum install -y php70-devel php70-mysql php70-mysqli php70-pdo \
      php70-pear php70-mbstring php70-cli php70-opcache php70-mcrypt \
      php70-intl php70-gd php70-xml php70-soap \
      php70-pecl-memcache php70-pecl-memcached && yum clean all

RUN yum install -y mysql56-server mysql56  && yum clean all

# Any logs you need forward here
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
#RUN ln -sf /dev/stderr /var/log/yum.log
#RUN ln -sf /dev/stderr /var/log/php-fpm/7.0/error.log

# Copy code (volume will be linked - this one - not!)

RUN mkdir /usr/share/www
ADD ./code /usr/share/www
RUN chown -R apache:apache /usr/share/www
RUN chmod -R 755 /usr/share/www

# add any settings you need
ADD docker/etc /etc

WORKDIR /usr/share/www/

EXPOSE 80

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
CMD service php-fpm start && service nginx start
#CMD ["nginx", "-g", "daemon off;"]